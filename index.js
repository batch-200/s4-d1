'use strict'

//Helper functions:
//validate array
const validArray = (arr) => {
  if (Array.isArray(arr)) {
    if (arr.length === 4) {
      if (arr.every((e) => e >= 0 && e <= 100)) {
        return arr
      } else {
        return undefined
      }
    } else {
      return undefined
    }
  } else if (typeof arr === 'Object') {
    validArray(Object.values(arr))
  } else {
    return undefined
  }
}

//validate Student object
const isStudentInstance = (obj) => {
  return obj instanceof Student ? true : false
  // if (obj instanceof Student) {
  //   return true
  // } else {
  //   return undefined
  // }
}

class Student {
  constructor(name, email, grades) {
    this._name = name
    this._email = email
    this._grades = validArray(grades)
    this._avg = undefined
    this._passed = undefined
    this._withHonors = undefined
  }

  login() {
    console.log(`${this._email} has logged in.`)
    return this
  }

  logout() {
    console.log(`${this._email} has logged out.`)
    return this
  }

  listGrades() {
    console.log(`${this._name}'s quarterly averages are: ${this._grades}`)
    return this
  }

  // compute for their grade average
  computeAverage() {
    this._avg =
      this._grades.reduce((acc, cur) => acc + cur, 0) / this._grades.length
    this._passed = this._avg >= 85 ? true : false
    // console.log(this._avg)
    return this
  }

  printAvg() {
    console.log(`average: ${this._avg}`)
    return this
  }

  // determine if student avg is >= 85
  willPass() {
    this._passed = this.computeAverage() >= 85 ? true : false
    console.log(`passed: ${this._passed}`)
    return this
  }

  // >=90 with true, >=85 & <90 false, <85 undefinded
  //activity - make it chaneable
  willPassWithHonors() {
    this.computeAverage()
    const avg = this._avg
    switch (true) {
      case avg >= 90:
        this._withHonors = true
        break

      case avg >= 85 && avg < 90:
        this._withHonors = false
        break

      // avg < 85 && avg > 100
      default:
        this._withHonors = undefined
        break
    }
    console.log(`with honors: ${this._withHonors}`)
    return this
  }
}

class Section {
  constructor(section) {
    this._section = section || undefined
    this._students = []
    this._honorStudents = 0
    this._honorsPercentage = undefined
  }

  // simple add student - add one by one
  addStudent(name, email, grades) {
    this._students.push(new Student(name, email, grades))
    return this
  }

  // count honor students
  // run the method, then get the value of the variable
  countHonorStudents() {
    this._honorStudents = 0

    this._students.forEach(
      (student) =>
        student.willPassWithHonors()._withHonors && this._honorStudents++
    )
    return this
  }

  // get honor students over number of students in percentage
  computeHonorsPercentage() {
    this._honorStudents = 0

    this.countHonorStudents()
    this._honorsPercentage = (this._honorStudents / this._students.length) * 100
    return this
  }
}

class Grade {
  constructor(level) {
    // super()
    this._level = level
    this._sections = []
    this._totalStudents = 0
    this._totalHonorStudents = 0
    this._batchAveGrade = 0
    this._batchMinGrade = 100
    this._batchMaxGrade = 0
  }

  //add section
  //will accept instance of Section
  addSection(section) {
    if (section instanceof Section) {
      this._sections.push(new Section(section))
    } else {
      throw new Error('Accepts instance of Section only!')
    }
    return this
  }

  // define a countStudents() method that will iterate over every section in the grade level,
  // incrementing the totalStudents property of the grade level object for every student found in every section.
  countStudents() {
    this._sections.forEach((section) => {
      this._totalStudents += section._section._students.length
    })
    return this
  }

  //Define a countHonorStudents() method that will perform similarly to countStudents()
  //except that it will only consider honor students when incrementing the totalHonorStudents property.
  countHonorStudents() {
    this._sections.forEach((section) => {
      this._totalHonorStudents += section._section._honorStudents
    })
    return this
  }

  //Define a computeBatchAve() method that will get the average of all the students' grade averages
  //and divide it by the total number of students in the grade level. The batchAveGrade property will be updated with the result of this operation.
  computeBatchAve() {
    let totalAvg = 0
    this._sections.forEach((section) => {
      section._section._students.forEach(
        (student) => (totalAvg += student._avg)
      )
    })
    this._batchAveGrade = totalAvg / this._totalStudents
  }

  //Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade scored by a student
  //of this grade level regardless of section.
  getBatchMinGrade() {
    this._sections.forEach((section) => {
      section._section._students.forEach((student) => {
        let minGrade = student._grades.reduce((a, b) => Math.min(a, b))
        this._batchMinGrade =
          this._batchMinGrade > minGrade ? minGrade : this._batchMinGrade
      })
    })
  }

  getBatchMaxGrade() {
    this._sections.forEach((section) => {
      section._section._students.forEach((student) => {
        let maxGrade = student._grades.reduce((a, b) => Math.max(a, b))

        this._batchMaxGrade =
          maxGrade > this._batchMaxGrade ? maxGrade : this._batchMaxGrade
      })
    })
  }
}

/**
 * const students = []
students.push(new Student('s1_name', 's1_email', [90, 90, 90, 90]))
students.push(new Student('s2_name', 's2_email', [80, 80, 80, 80]))
students.push(new Student('s3_name', 's3_email', [85, 85, 85, 85]))
students.push(new Student('s3_name', 's3_email', [80, 80, 85, 85]))
console.log(students)
const x = new Student('s3_name', 's3_email', [85, 85, 85, 85])
console.log(x instanceof Student)

 */

// class section test
const section1 = new Section('section 1')
section1.addStudent('s1_name', 's1_email', [90, 90, 90, 90])
section1.addStudent('s1_name', 's1_email', [90, 90, 90, 90])
section1.addStudent('s1_name', 's1_email', [90, 90, 90, 90])
section1.addStudent('s1_name', 's1_email', [89, 89, 89, 89])
section1.addStudent('s3_name', 's3_email', [50, 50, 85, 85])
console.log(section1)
console.log(section1.countHonorStudents())
console.log(section1.computeHonorsPercentage())

// class grade test
const g = new Grade('1')
console.log(g)
g.addSection(section1)
g.addSection(section1)
g.countStudents()
g.countHonorStudents()
g.computeBatchAve()
g.getBatchMinGrade()
g.getBatchMaxGrade()
console.log(g)
